package com.anirudh.smsreader.feature_sms.presentation.util

import android.R
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.telephony.SmsMessage
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.core.app.NotificationCompat
import androidx.navigation.NavDeepLinkBuilder
import com.anirudh.smsreader.feature_sms.domain.model.SMSEntity
import com.anirudh.smsreader.feature_sms.presentation.MainActivity
import com.anirudh.smsreader.feature_sms.presentation.util.Constants.SMS_DATA
import com.google.gson.Gson


class SMSNotification : BroadcastReceiver() {
    @RequiresApi(Build.VERSION_CODES.O)
    @ExperimentalAnimationApi
    @ExperimentalFoundationApi
    override fun onReceive(context: Context?, intent: Intent?) {
        val bundle = intent!!.extras
        Log.e("TAG", "onReceive: bundle: $bundle ")
        if (bundle != null && bundle.containsKey("pdus")) {
            val pdus = bundle["pdus"] as Array<*>?
            val sms: SmsMessage = SmsMessage.createFromPdu(pdus!![0] as ByteArray)
            val senderNumber: String = sms.displayOriginatingAddress
            val body: String = sms.displayMessageBody
            val timestamp = sms.timestampMillis

            val notificationManager =
                context!!.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val notificationChannel = NotificationChannel(
                "CHANNEL_ID",
                "CHANNEL_NAME",
                NotificationManager.IMPORTANCE_HIGH
            )
            notificationManager.createNotificationChannel(notificationChannel)

            val smsEntity = SMSEntity(senderNumber, body, timestamp)
            val smsEntityString = Gson().toJson(smsEntity)

            val notificationIntent = Intent(context, MainActivity::class.java)
            notificationIntent.putExtra(SMS_DATA, smsEntityString)
            Log.e("TAG", "onReceive: $smsEntityString")

            val notification: Notification = NotificationCompat.Builder(context, "CHANNEL_ID")
                .setContentTitle("senderNumber: $senderNumber")
                .setContentText("body: $body")
                .setSmallIcon(R.drawable.btn_star)
                .setContentIntent(
                    PendingIntent.getActivity(
                        context,
                        0,
                        notificationIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                    )
                )
                .build()
            notificationManager.notify(0, notification)
        }
    }

}