package com.anirudh.smsreader.feature_sms.presentation.message.SMSDetailsScreen

import android.util.Log
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.anirudh.smsreader.feature_sms.domain.model.SMSEntity

@Composable
fun SMSDetailsScreen(
    navController: NavController,
    smsEntity: SMSEntity
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
    ) {
//        Row(
//            modifier = Modifier
//                .fillMaxWidth()
//                .padding(8.dp),
//            horizontalArrangement = Arrangement.SpaceBetween
//        ) {
//        }
        Log.e("TAG", "SMSDetailsScreen: " + smsEntity)
        Spacer(modifier = Modifier.height(16.dp))
        Text(text = smsEntity.messageContact)
        Spacer(modifier = Modifier.height(16.dp))
        Text(text = smsEntity.messageData)
        Spacer(modifier = Modifier.height(16.dp))
        Text(text = smsEntity.timestamp.toString())
    }

}