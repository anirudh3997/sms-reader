package com.anirudh.smsreader.feature_sms.domain.use_case

import com.anirudh.smsreader.feature_sms.domain.model.SMSEntity
import com.anirudh.smsreader.feature_sms.domain.repository.MessagesRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class GetMessagesUseCase(private val repository: MessagesRepository) {
    operator fun invoke(): Flow<List<SMSEntity>> {
        return repository.getMessages().map { notes -> notes.sortedByDescending { it.timestamp } }
    }
}