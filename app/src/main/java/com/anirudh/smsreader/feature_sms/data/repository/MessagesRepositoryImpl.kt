package com.anirudh.smsreader.feature_sms.data.repository

import android.content.ContentResolver
import android.content.Context
import android.database.Cursor
import android.provider.Telephony
import com.anirudh.smsreader.feature_sms.domain.model.SMSEntity
import com.anirudh.smsreader.feature_sms.domain.repository.MessagesRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow


class MessagesRepositoryImpl(private val context: Context) : MessagesRepository {
    override fun getMessages(): Flow<List<SMSEntity>> {
        return flow {
            emit(getAllSms(context = context))
        }
    }

    private fun getAllSms(context: Context): List<SMSEntity> {
        val smsList: ArrayList<SMSEntity> = ArrayList()
        val cr: ContentResolver = context.contentResolver
        val c: Cursor? = cr.query(Telephony.Sms.CONTENT_URI, null, null, null, null)
        val totalSMS: Int
        if (c != null) {
            totalSMS = c.count
            if (c.moveToFirst()) {
                for (j in 0 until totalSMS) {
                    val smsDate: String =
                        c.getString(c.getColumnIndexOrThrow(Telephony.Sms.DATE))
                    val number: String =
                        c.getString(c.getColumnIndexOrThrow(Telephony.Sms.ADDRESS))
                    val body: String = c.getString(c.getColumnIndexOrThrow(Telephony.Sms.BODY))
                    smsList.add(SMSEntity(number, body, smsDate.toLong()))
                    c.moveToNext()
                }
            }
            c.close()
        }
        return smsList
    }
}