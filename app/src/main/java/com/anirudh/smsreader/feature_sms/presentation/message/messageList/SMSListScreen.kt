package com.anirudh.smsreader.feature_sms.presentation.message.messageList

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.anirudh.smsreader.feature_sms.presentation.message.components.SMSItem
import com.anirudh.smsreader.feature_sms.presentation.util.Screen
import com.google.gson.Gson
import java.util.*

@ExperimentalFoundationApi
@ExperimentalAnimationApi
@Composable
fun SMSListScreen(
    navController: NavController,
    viewModel: SMSViewModel = hiltViewModel()
) {
    val state = viewModel.state.value
    val scaffoldState = rememberScaffoldState()

    Scaffold(
        scaffoldState = scaffoldState
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
        ) {
            Spacer(modifier = Modifier.height(16.dp))
            LazyColumn(modifier = Modifier.fillMaxSize()) {
                val grouped = state.SMSEntities.groupBy { item ->
                    val diff: Long = Date().time - item.timestamp
                    val seconds = diff / 1000
                    val minutes = seconds / 60
                    when (val hours = minutes / 60) {
                        in 0L..3L -> return@groupBy "$hours hours ago"
                        in 3L..6L -> return@groupBy "6 hours ago"
                        in 6L..12L -> return@groupBy "12 hours ago"
                        else -> return@groupBy "1 day ago"
                    }
                }

                grouped.forEach { (section, sectionPersons) ->
                    stickyHeader {
                        Text(
                            text = section,
                            color = MaterialTheme.colors.primary,
                            modifier = Modifier
                                .background(MaterialTheme.colors.background)
                                .padding(16.dp, 8.dp)
                                .fillMaxWidth()
                        )
                    }

                    items(
                        items = sectionPersons,
                        itemContent = {
                            SMSItem(
                                SMSEntity = it,
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(8.dp)
                                    .clickable {
                                        val smsEntityString = Gson().toJson(it)
                                        navController.navigate(
                                            Screen.SMSDetailsScreen.route + "?smsEntity=${smsEntityString}"
                                        )
                                    }
                            )
                        }
                    )
                }
            }
        }
    }
}