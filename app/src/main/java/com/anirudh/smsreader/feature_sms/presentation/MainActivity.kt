package com.anirudh.smsreader.feature_sms.presentation

import android.Manifest
import android.content.Context
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.core.app.ActivityCompat
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.anirudh.smsreader.feature_sms.domain.model.SMSEntity
import com.anirudh.smsreader.feature_sms.presentation.message.SMSDetailsScreen.SMSDetailsScreen
import com.anirudh.smsreader.feature_sms.presentation.message.messageList.SMSListScreen
import com.anirudh.smsreader.feature_sms.presentation.util.Constants.SMS_DATA
import com.anirudh.smsreader.feature_sms.presentation.util.SMSNotification
import com.anirudh.smsreader.feature_sms.presentation.util.Screen
import com.anirudh.smsreader.ui.theme.SMSReaderTheme
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import java.lang.Exception


@ExperimentalFoundationApi
@ExperimentalAnimationApi
@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            SMSReaderTheme {
                CheckSMSPermission()
                if (this::isGranted.isInitialized && isGranted.value == true) {
                    MainComposeLayout()
                }
            }
        }
    }

    private lateinit var isGranted: MutableState<Boolean?>

    @Composable
    private fun MainComposeLayout() {
        registerSmsListener()
        Surface(
            color = MaterialTheme.colors.background
        ) {
            val navController = rememberNavController()
            NavHost(
                navController = navController,
                startDestination = Screen.SMSListScreen.route
            ) {
                composable(route = Screen.SMSListScreen.route) {
                    SMSListScreen(navController = navController)
                }
                composable(
                    route = Screen.SMSDetailsScreen.route + "?smsEntity={smsEntity}",
                    arguments = listOf(
                        navArgument(name = "smsEntity") {
                            type = NavType.StringType
                        }
                    )
                ) {
                    val smsEntityString = it.arguments?.getString("smsEntity")
                    val smsEntity = Gson().fromJson(smsEntityString, SMSEntity::class.java)
                    if (smsEntity != null) {
                        SMSDetailsScreen(navController = navController, smsEntity = smsEntity)
                    }
                }
            }
        }
    }

    private fun registerSmsListener() {
        val filter = IntentFilter()
        filter.addAction("android.provider.Telephony.SMS_RECEIVED")
        val receiver = SMSNotification()
        registerReceiver(receiver, filter)
    }

    @Composable
    private fun CheckSMSPermission() {
        isGranted = remember { mutableStateOf(false) }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            MainComposeLayout()
        }
        this.let {
            if (hasPermissions(this, PERMISSIONS)) {
                MainComposeLayout()
            } else {
                permReqLauncher.launch(
                    PERMISSIONS
                )
            }
        }
    }

    private fun hasPermissions(context: Context, permissions: Array<String>): Boolean =
        permissions.all {
            ActivityCompat.checkSelfPermission(context, it) == PackageManager.PERMISSION_GRANTED
        }

    private val permReqLauncher =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
            val granted = permissions.entries.all {
                it.value
            }
            isGranted.value = granted
        }

    companion object {
        private var PERMISSIONS = arrayOf(
            Manifest.permission.RECEIVE_SMS,
            Manifest.permission.READ_SMS,
        )
    }

}
