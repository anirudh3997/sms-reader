package com.anirudh.smsreader.feature_sms.presentation.message.messageList

import com.anirudh.smsreader.feature_sms.domain.model.SMSEntity

data class SMSState(
    val SMSEntities: List<SMSEntity> = emptyList(),
)
