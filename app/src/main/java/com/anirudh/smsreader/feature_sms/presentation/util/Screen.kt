package com.anirudh.smsreader.feature_sms.presentation.util

sealed class Screen(val route: String) {
    object SMSListScreen : Screen("sms_list_screen")
    object SMSDetailsScreen : Screen("sms_detail_screen")
}
