package com.anirudh.smsreader.feature_sms.presentation.message.messageList

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.anirudh.smsreader.feature_sms.domain.use_case.SMSUseCases
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class SMSViewModel @Inject constructor(
    private val SMSUseCase: SMSUseCases
) : ViewModel() {

    private val _state = mutableStateOf(SMSState())
    val state: State<SMSState> = _state

    private var getMessagesJob: Job? = null

    init {
        getNotes()
    }

    private fun getNotes() {
        getMessagesJob?.cancel()
        getMessagesJob = SMSUseCase.getMessagesUseCase().onEach { smsList ->
            _state.value = state.value.copy(SMSEntities = smsList)
        }.launchIn(viewModelScope)
    }
}