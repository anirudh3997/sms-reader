package com.anirudh.smsreader.feature_sms.domain.repository

import com.anirudh.smsreader.feature_sms.domain.model.SMSEntity
import kotlinx.coroutines.flow.Flow

interface MessagesRepository {
    fun getMessages(): Flow<List<SMSEntity>>
}