package com.anirudh.smsreader.feature_sms.domain.model

data class SMSEntity(
    val messageContact: String,
    val messageData: String,
    val timestamp: Long
)