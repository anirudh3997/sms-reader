package com.anirudh.smsreader.feature_sms.domain.use_case

data class SMSUseCases(
    val getMessagesUseCase: GetMessagesUseCase,
)