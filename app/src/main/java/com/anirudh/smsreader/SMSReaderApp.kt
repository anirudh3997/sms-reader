package com.anirudh.smsreader

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SMSReaderApp : Application()