package com.anirudh.smsreader.di

import android.app.Application
import com.anirudh.smsreader.feature_sms.data.repository.MessagesRepositoryImpl
import com.anirudh.smsreader.feature_sms.domain.repository.MessagesRepository
import com.anirudh.smsreader.feature_sms.domain.use_case.GetMessagesUseCase
import com.anirudh.smsreader.feature_sms.domain.use_case.SMSUseCases
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideNoteRepository(app: Application): MessagesRepository {
        return MessagesRepositoryImpl(app.applicationContext)
    }

    @Provides
    @Singleton
    fun provideNoteUseCases(repository: MessagesRepository): SMSUseCases {
        return SMSUseCases(
            getMessagesUseCase = GetMessagesUseCase(repository),
        )
    }
}